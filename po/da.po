# Danish language file for gnome-shell-extension-openweather.
# Copyright (C) 2112
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# Michael Rasmussen <mir@datanom.net>, 2012.
#
msgid ""
msgstr ""
"Project-Id-Version: 1.4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-06-23 14:51-0300\n"
"PO-Revision-Date: 2012-04-07 03:07+0200\n"
"Last-Translator: Michael Rasmussen <mir@datanom.net>\n"
"Language-Team: Danish <>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"

#: src/extension.js:267
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""

#: src/extension.js:454 src/extension.js:466
#, javascript-format
msgid "Can not connect to %s"
msgstr ""

#: src/extension.js:769 src/preferences/locationsPage.js:39
#: src/preferences/locationsPage.js:58
msgid "Locations"
msgstr ""

#: src/extension.js:770
msgid "Reload Weather Information"
msgstr ""

#: src/extension.js:771
#, javascript-format
msgid "Weather data by: %s"
msgstr ""

#: src/extension.js:773
msgid "Weather Settings"
msgstr ""

#: src/extension.js:787
msgid "Manual refreshes less than 2 minutes apart are ignored!"
msgstr ""

#: src/extension.js:801
#, javascript-format
msgid "Can not open %s"
msgstr ""

#: src/extension.js:849 src/preferences/locationsPage.js:509
msgid "Invalid city"
msgstr ""

#: src/extension.js:860
msgid "Invalid location! Please try to recreate it."
msgstr ""

#: src/extension.js:906 src/preferences/generalPage.js:148
msgid "°F"
msgstr ""

#: src/extension.js:908 src/preferences/generalPage.js:149
msgid "K"
msgstr ""

#: src/extension.js:910 src/preferences/generalPage.js:150
msgid "°Ra"
msgstr ""

#: src/extension.js:912 src/preferences/generalPage.js:151
msgid "°Ré"
msgstr ""

#: src/extension.js:914 src/preferences/generalPage.js:152
msgid "°Rø"
msgstr ""

#: src/extension.js:916 src/preferences/generalPage.js:153
msgid "°De"
msgstr ""

#: src/extension.js:918 src/preferences/generalPage.js:154
msgid "°N"
msgstr ""

#: src/extension.js:920 src/preferences/generalPage.js:147
msgid "°C"
msgstr ""

#: src/extension.js:957
msgid "Calm"
msgstr ""

#: src/extension.js:960
msgid "Light air"
msgstr ""

#: src/extension.js:963
msgid "Light breeze"
msgstr ""

#: src/extension.js:966
msgid "Gentle breeze"
msgstr ""

#: src/extension.js:969
msgid "Moderate breeze"
msgstr ""

#: src/extension.js:972
msgid "Fresh breeze"
msgstr ""

#: src/extension.js:975
msgid "Strong breeze"
msgstr ""

#: src/extension.js:978
msgid "Moderate gale"
msgstr ""

#: src/extension.js:981
msgid "Fresh gale"
msgstr ""

#: src/extension.js:984
msgid "Strong gale"
msgstr ""

#: src/extension.js:987
msgid "Storm"
msgstr ""

#: src/extension.js:990
msgid "Violent storm"
msgstr ""

#: src/extension.js:993
msgid "Hurricane"
msgstr "Tyfon"

#: src/extension.js:997
msgid "Sunday"
msgstr "Søndag"

#: src/extension.js:997
msgid "Monday"
msgstr "Mandag"

#: src/extension.js:997
msgid "Tuesday"
msgstr "Tirsdag"

#: src/extension.js:997
msgid "Wednesday"
msgstr "Onsdag"

#: src/extension.js:997
msgid "Thursday"
msgstr "Torsdag"

#: src/extension.js:997
msgid "Friday"
msgstr "Fredag"

#: src/extension.js:997
msgid "Saturday"
msgstr "Lørdag"

#: src/extension.js:1003
msgid "N"
msgstr "N"

#: src/extension.js:1003
msgid "NE"
msgstr "NØ"

#: src/extension.js:1003
msgid "E"
msgstr "Ø"

#: src/extension.js:1003
msgid "SE"
msgstr "SØ"

#: src/extension.js:1003
msgid "S"
msgstr "S"

#: src/extension.js:1003
msgid "SW"
msgstr "SV"

#: src/extension.js:1003
msgid "W"
msgstr "V"

#: src/extension.js:1003
msgid "NW"
msgstr "NV"

#: src/extension.js:1056 src/extension.js:1065
#: src/preferences/generalPage.js:177
msgid "hPa"
msgstr ""

#: src/extension.js:1060 src/preferences/generalPage.js:178
msgid "inHg"
msgstr ""

#: src/extension.js:1070 src/preferences/generalPage.js:179
msgid "bar"
msgstr ""

#: src/extension.js:1075 src/preferences/generalPage.js:180
msgid "Pa"
msgstr ""

#: src/extension.js:1080 src/preferences/generalPage.js:181
msgid "kPa"
msgstr ""

#: src/extension.js:1085 src/preferences/generalPage.js:182
msgid "atm"
msgstr ""

#: src/extension.js:1090 src/preferences/generalPage.js:183
msgid "at"
msgstr ""

#: src/extension.js:1095 src/preferences/generalPage.js:184
msgid "Torr"
msgstr ""

#: src/extension.js:1100 src/preferences/generalPage.js:185
msgid "psi"
msgstr ""

#: src/extension.js:1105 src/preferences/generalPage.js:186
msgid "mmHg"
msgstr ""

#: src/extension.js:1110 src/preferences/generalPage.js:187
msgid "mbar"
msgstr ""

#: src/extension.js:1158 src/preferences/generalPage.js:165
msgid "m/s"
msgstr ""

#: src/extension.js:1163 src/preferences/generalPage.js:164
msgid "mph"
msgstr ""

#: src/extension.js:1168 src/preferences/generalPage.js:163
msgid "km/h"
msgstr ""

#: src/extension.js:1177 src/preferences/generalPage.js:166
msgid "kn"
msgstr ""

#: src/extension.js:1182 src/preferences/generalPage.js:167
msgid "ft/s"
msgstr ""

#: src/extension.js:1227 src/extension.js:1265
msgid "Loading ..."
msgstr "Henter ..."

#: src/extension.js:1269
msgid "Please wait"
msgstr "Vent venligst"

#: src/extension.js:1340
msgid "Feels Like:"
msgstr ""

#: src/extension.js:1344
msgid "Humidity:"
msgstr "Fugtighed:"

#: src/extension.js:1348
msgid "Pressure:"
msgstr "Tryk:"

#: src/extension.js:1352
msgid "Wind:"
msgstr "Vind:"

#: src/extension.js:1356
msgid "Gusts:"
msgstr ""

#: src/extension.js:1487
msgid "Tomorrow's Forecast"
msgstr ""

#: src/extension.js:1489
#, javascript-format
msgid "%s Day Forecast"
msgstr ""

#: src/openweathermap.js:123
#, fuzzy
msgid "Thunderstorm with Light Rain"
msgstr "Tordenvejr"

#: src/openweathermap.js:125
#, fuzzy
msgid "Thunderstorm with Rain"
msgstr "Tordenvejr"

#: src/openweathermap.js:127
#, fuzzy
msgid "Thunderstorm with Heavy Rain"
msgstr "Tordenvejr"

#: src/openweathermap.js:129
#, fuzzy
msgid "Light Thunderstorm"
msgstr "Lokale tordenbyger"

#: src/openweathermap.js:131
#, fuzzy
msgid "Thunderstorm"
msgstr "Tordenvejr"

#: src/openweathermap.js:133
#, fuzzy
msgid "Heavy Thunderstorm"
msgstr "Kraftigt tordenvejr"

#: src/openweathermap.js:135
#, fuzzy
msgid "Ragged Thunderstorm"
msgstr "Lokale tordenbyger"

#: src/openweathermap.js:137
#, fuzzy
msgid "Thunderstorm with Light Drizzle"
msgstr "Tordenvejr"

#: src/openweathermap.js:139
#, fuzzy
msgid "Thunderstorm with Drizzle"
msgstr "Tordenvejr"

#: src/openweathermap.js:141
#, fuzzy
msgid "Thunderstorm with Heavy Drizzle"
msgstr "Tordenvejr"

#: src/openweathermap.js:143
#, fuzzy
msgid "Light Drizzle"
msgstr "Dugregn"

#: src/openweathermap.js:145
#, fuzzy
msgid "Drizzle"
msgstr "Dugregn"

#: src/openweathermap.js:147
#, fuzzy
msgid "Heavy Drizzle"
msgstr "Dugregn"

#: src/openweathermap.js:149
#, fuzzy
msgid "Light Drizzle Rain"
msgstr "Dugregn"

#: src/openweathermap.js:151
#, fuzzy
msgid "Drizzle Rain"
msgstr "Dugregn"

#: src/openweathermap.js:153
#, fuzzy
msgid "Heavy Drizzle Rain"
msgstr "Dugregn"

#: src/openweathermap.js:155
#, fuzzy
msgid "Shower Rain and Drizzle"
msgstr "Regn og hagl"

#: src/openweathermap.js:157
#, fuzzy
msgid "Heavy Rain and Drizzle"
msgstr "Regn og hagl"

#: src/openweathermap.js:159
#, fuzzy
msgid "Shower Drizzle"
msgstr "Frysende dugregn"

#: src/openweathermap.js:161
#, fuzzy
msgid "Light Rain"
msgstr "Isslag"

#: src/openweathermap.js:163
#, fuzzy
msgid "Moderate Rain"
msgstr "Dugregn"

#: src/openweathermap.js:165
#, fuzzy
msgid "Heavy Rain"
msgstr "Kraftig sne"

#: src/openweathermap.js:167
msgid "Very Heavy Rain"
msgstr ""

#: src/openweathermap.js:169
#, fuzzy
msgid "Extreme Rain"
msgstr "Isslag"

#: src/openweathermap.js:171
#, fuzzy
msgid "Freezing Rain"
msgstr "Isslag"

#: src/openweathermap.js:173
#, fuzzy
msgid "Light Shower Rain"
msgstr "Lette snebyger"

#: src/openweathermap.js:175
#, fuzzy
msgid "Shower Rain"
msgstr "Byger"

#: src/openweathermap.js:177
#, fuzzy
msgid "Heavy Shower Rain"
msgstr "Kraftig sne"

#: src/openweathermap.js:179
#, fuzzy
msgid "Ragged Shower Rain"
msgstr "Byger"

#: src/openweathermap.js:181
#, fuzzy
msgid "Light Snow"
msgstr "Snestorm"

#: src/openweathermap.js:183
msgid "Snow"
msgstr ""

#: src/openweathermap.js:185
#, fuzzy
msgid "Heavy Snow"
msgstr "Kraftig sne"

#: src/openweathermap.js:187
#, fuzzy
msgid "Sleet"
msgstr "Slud"

#: src/openweathermap.js:189
#, fuzzy
msgid "Light Shower Sleet"
msgstr "Byger"

#: src/openweathermap.js:191
#, fuzzy
msgid "Shower Sleet"
msgstr "Byger"

#: src/openweathermap.js:193
#, fuzzy
msgid "Light Rain and Snow"
msgstr "Regn og slud"

#: src/openweathermap.js:195
#, fuzzy
msgid "Rain and Snow"
msgstr "Regn og slud"

#: src/openweathermap.js:197
#, fuzzy
msgid "Light Shower Snow"
msgstr "Lette snebyger"

#: src/openweathermap.js:199
#, fuzzy
msgid "Shower Snow"
msgstr "Byger"

#: src/openweathermap.js:201
#, fuzzy
msgid "Heavy Shower Snow"
msgstr "Kraftig sne"

#: src/openweathermap.js:203
msgid "Mist"
msgstr ""

#: src/openweathermap.js:205
msgid "Smoke"
msgstr ""

#: src/openweathermap.js:207
msgid "Haze"
msgstr ""

#: src/openweathermap.js:209
msgid "Sand/Dust Whirls"
msgstr ""

#: src/openweathermap.js:211
#, fuzzy
msgid "Fog"
msgstr "Tåge"

#: src/openweathermap.js:213
msgid "Sand"
msgstr ""

#: src/openweathermap.js:215
msgid "Dust"
msgstr ""

#: src/openweathermap.js:217
msgid "Volcanic Ash"
msgstr ""

#: src/openweathermap.js:219
msgid "Squalls"
msgstr ""

#: src/openweathermap.js:221
msgid "Tornado"
msgstr ""

#: src/openweathermap.js:223
#, fuzzy
msgid "Clear Sky"
msgstr "Klart"

#: src/openweathermap.js:225
#, fuzzy
msgid "Few Clouds"
msgstr "Næsten overskyet"

#: src/openweathermap.js:227
#, fuzzy
msgid "Scattered Clouds"
msgstr "Spredte regnbyger"

#: src/openweathermap.js:229
#, fuzzy
msgid "Broken Clouds"
msgstr "Næsten overskyet"

#: src/openweathermap.js:231
#, fuzzy
msgid "Overcast Clouds"
msgstr "Næsten overskyet"

#: src/openweathermap.js:233
msgid "Not available"
msgstr "Ikke tilgængelig"

#: src/openweathermap.js:447 src/openweathermap.js:449
msgid ", "
msgstr ""

#: src/openweathermap.js:465
msgid "?"
msgstr ""

#: src/openweathermap.js:537
msgid "Tomorrow"
msgstr "I morgen"

#: src/preferences/generalPage.js:31
msgid "Settings"
msgstr ""

#: src/preferences/generalPage.js:39
msgid "General"
msgstr ""

#: src/preferences/generalPage.js:57
msgid "Current Weather Refresh"
msgstr ""

#: src/preferences/generalPage.js:58
msgid "Current weather refresh interval in minutes"
msgstr ""

#: src/preferences/generalPage.js:80
msgid "Weather Forecast Refresh"
msgstr ""

#: src/preferences/generalPage.js:81
msgid "Forecast refresh interval in minutes if enabled"
msgstr ""

#: src/preferences/generalPage.js:92
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:108
msgid "Disable Forecast"
msgstr ""

#: src/preferences/generalPage.js:93
msgid "Disables all fetching and processing of forecast data"
msgstr ""

#: src/preferences/generalPage.js:104
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:96
msgid "System Icons"
msgstr ""

#: src/preferences/generalPage.js:105
msgid "Disable to use packaged Adwaita weather icons"
msgstr ""

#: src/preferences/generalPage.js:106
msgid ""
"If you have issues with your system icons displaying correctly disable this "
"to fix it"
msgstr ""

#: src/preferences/generalPage.js:126
msgid "First Boot Delay"
msgstr ""

#: src/preferences/generalPage.js:127
msgid "Seconds to delay popup initialization and data fetching"
msgstr ""

#: src/preferences/generalPage.js:128
msgid ""
"This setting only applies to the first time the extension is loaded. (first "
"log in / restarting gnome shell)"
msgstr ""

#: src/preferences/generalPage.js:142
msgid "Units"
msgstr ""

#: src/preferences/generalPage.js:156
#, fuzzy
msgid "Temperature"
msgstr "Temperatur"

#: src/preferences/generalPage.js:168
msgid "Beaufort"
msgstr ""

#: src/preferences/generalPage.js:170
msgid "Wind Speed"
msgstr ""

#: src/preferences/generalPage.js:189
#, fuzzy
msgid "Pressure"
msgstr "Tryk:"

#: src/preferences/generalPage.js:201 src/preferences/locationsPage.js:66
msgid "Provider"
msgstr ""

#: src/preferences/generalPage.js:210
msgid "OpenWeatherMap Multilingual Support"
msgstr ""

#: src/preferences/generalPage.js:211
msgid "Using provider translations applies to weather conditions only"
msgstr ""

#: src/preferences/generalPage.js:212
msgid ""
"Enable this to use OWM multilingual support in 46 languages if there's no "
"built-in translations for your language yet."
msgstr ""

#: src/preferences/generalPage.js:224
msgid "Use Extensions API Key"
msgstr ""

#: src/preferences/generalPage.js:225
msgid "Use the built-in API key for OpenWeatherMap"
msgstr ""

#: src/preferences/generalPage.js:226
msgid ""
"Disable this if you have your own API key from openweathermap.org and enter "
"it below."
msgstr ""

#: src/preferences/generalPage.js:240
msgid "Personal API Key"
msgstr ""

#: src/preferences/layoutPage.js:31
msgid "Layout"
msgstr ""

#: src/preferences/layoutPage.js:39
msgid "Panel"
msgstr ""

#: src/preferences/layoutPage.js:44
msgid "Center"
msgstr ""

#: src/preferences/layoutPage.js:45
msgid "Right"
msgstr ""

#: src/preferences/layoutPage.js:46
msgid "Left"
msgstr ""

#: src/preferences/layoutPage.js:48
msgid "Position In Panel"
msgstr ""

#: src/preferences/layoutPage.js:69
msgid "Position Offset"
msgstr ""

#: src/preferences/layoutPage.js:70
msgid "The position relative to other items in the box"
msgstr ""

#: src/preferences/layoutPage.js:78
#, fuzzy
msgid "Show the temperature in the panel"
msgstr "Temperatur"

#: src/preferences/layoutPage.js:82
#, fuzzy
msgid "Temperature In Panel"
msgstr "Temperatur"

#: src/preferences/layoutPage.js:90
msgid "Show the weather conditions in the panel"
msgstr ""

#: src/preferences/layoutPage.js:94
msgid "Conditions In Panel"
msgstr ""

#: src/preferences/layoutPage.js:107
msgid "Popup"
msgstr ""

#: src/preferences/layoutPage.js:125
msgid "Popup Position"
msgstr ""

#: src/preferences/layoutPage.js:126
msgid "Alignment of the popup from left to right"
msgstr ""

#: src/preferences/layoutPage.js:137
msgid "Wind Direction Arrows"
msgstr ""

#: src/preferences/layoutPage.js:149
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:88
msgid "Translate Conditions"
msgstr ""

#: src/preferences/layoutPage.js:157
msgid "0"
msgstr ""

#: src/preferences/layoutPage.js:158 src/preferences/layoutPage.js:229
msgid "1"
msgstr ""

#: src/preferences/layoutPage.js:159 src/preferences/layoutPage.js:230
msgid "2"
msgstr ""

#: src/preferences/layoutPage.js:160 src/preferences/layoutPage.js:231
msgid "3"
msgstr ""

#: src/preferences/layoutPage.js:162
#, fuzzy
msgid "Temperature Decimal Places"
msgstr "Temperatur"

#: src/preferences/layoutPage.js:163
msgid "Maximum number of digits after the decimal point"
msgstr ""

#: src/preferences/layoutPage.js:183
msgid "Location Text Length"
msgstr ""

#: src/preferences/layoutPage.js:184
msgid "Maximum length of the location text. A setting of '0' is unlimited"
msgstr ""

#: src/preferences/layoutPage.js:200
msgid "Forecast"
msgstr ""

#: src/preferences/layoutPage.js:210
msgid "Center Today's Forecast"
msgstr ""

#: src/preferences/layoutPage.js:221
msgid "Conditions In Forecast"
msgstr ""

#: src/preferences/layoutPage.js:228
msgid "Today Only"
msgstr ""

#: src/preferences/layoutPage.js:232
msgid "4"
msgstr ""

#: src/preferences/layoutPage.js:233
msgid "5"
msgstr ""

#: src/preferences/layoutPage.js:235
msgid "Total Days In Forecast"
msgstr ""

#: src/preferences/layoutPage.js:246
msgid "Keep Forecast Expanded"
msgstr ""

#: src/preferences/locationsPage.js:54
msgid "Add"
msgstr ""

#: src/preferences/locationsPage.js:73
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:58
msgid "Geolocation Provider"
msgstr ""

#: src/preferences/locationsPage.js:74
msgid "Provider used for location search"
msgstr ""

#: src/preferences/locationsPage.js:87
msgid "Personal MapQuest Key"
msgstr ""

#: src/preferences/locationsPage.js:88
msgid "Personal API Key from developer.mapquest.com"
msgstr ""

#: src/preferences/locationsPage.js:208
#, javascript-format
msgid "Location changed to: %s"
msgstr ""

#: src/preferences/locationsPage.js:223
msgid "Add New Location"
msgstr ""

#: src/preferences/locationsPage.js:244
msgid "Search by Location or Coordinates"
msgstr ""

#: src/preferences/locationsPage.js:250
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr ""

#: src/preferences/locationsPage.js:252 src/preferences/locationsPage.js:337
#: src/preferences/locationsPage.js:354
#, fuzzy
msgid "Clear entry"
msgstr "Klart"

#: src/preferences/locationsPage.js:261
#, fuzzy
msgid "Search"
msgstr "Henter ..."

#: src/preferences/locationsPage.js:284
msgid "We need something to search for!"
msgstr ""

#: src/preferences/locationsPage.js:307
#, javascript-format
msgid "Edit %s"
msgstr ""

#: src/preferences/locationsPage.js:329
msgid "Edit Name"
msgstr ""

#: src/preferences/locationsPage.js:345
msgid "Edit Coordinates"
msgstr ""

#: src/preferences/locationsPage.js:363
msgid "Save"
msgstr ""

#: src/preferences/locationsPage.js:396
msgid "Please complete all fields"
msgstr ""

#: src/preferences/locationsPage.js:412
#, javascript-format
msgid "%s has been updated"
msgstr ""

#: src/preferences/locationsPage.js:441
#, javascript-format
msgid "Are you sure you want to delete \"%s\"?"
msgstr ""

#: src/preferences/locationsPage.js:449
msgid "Delete"
msgstr ""

#: src/preferences/locationsPage.js:453
msgid "Cancel"
msgstr ""

#: src/preferences/locationsPage.js:485
#, javascript-format
msgid "%s has been deleted"
msgstr ""

#: src/preferences/locationsPage.js:531
msgid "Search Results"
msgstr ""

#: src/preferences/locationsPage.js:547
msgid "New Search"
msgstr ""

#: src/preferences/locationsPage.js:554
#, fuzzy
msgid "Searching ..."
msgstr "Henter ..."

#: src/preferences/locationsPage.js:555
#, javascript-format
msgid "Please wait while searching for locations matching \"%s\""
msgstr ""

#: src/preferences/locationsPage.js:607
msgid "AppKey Required"
msgstr ""

#: src/preferences/locationsPage.js:608
#, javascript-format
msgid "You need an AppKey to use MapQuest, get one at: %s"
msgstr ""

#: src/preferences/locationsPage.js:705
#, javascript-format
msgid "Results for \"%s\""
msgstr ""

#: src/preferences/locationsPage.js:752
#, javascript-format
msgid "%s has been added"
msgstr ""

#: src/preferences/locationsPage.js:760
msgid "API Error"
msgstr ""

#: src/preferences/locationsPage.js:761
#, javascript-format
msgid "Invalid data when searching for \"%s\"."
msgstr ""

#: src/preferences/locationsPage.js:764
msgid "No Matches Found"
msgstr ""

#: src/preferences/locationsPage.js:765
#, javascript-format
msgid "No results found when searching for \"%s\"."
msgstr ""

#: src/preferences/aboutPage.js:31
msgid "About"
msgstr ""

#: src/preferences/aboutPage.js:58
msgid ""
"Display weather information for any location on Earth in the GNOME Shell"
msgstr ""

#: src/preferences/aboutPage.js:72
msgid "unknown"
msgstr ""

#: src/preferences/aboutPage.js:78
msgid "OpenWeather Version"
msgstr ""

#: src/preferences/aboutPage.js:87
msgid "Git Version"
msgstr ""

#: src/preferences/aboutPage.js:95
msgid "GNOME Version"
msgstr ""

#: src/preferences/aboutPage.js:102
msgid "Session Type"
msgstr ""

#: src/preferences/aboutPage.js:124
#, javascript-format
msgid "Maintained by: %s"
msgstr ""

#: src/preferences/aboutPage.js:161
#, javascript-format
msgid "Weather data provided by: %s"
msgstr ""

#: src/preferences/aboutPage.js:172
msgid "This program comes with ABSOLUTELY NO WARRANTY."
msgstr ""

#: src/preferences/aboutPage.js:173
msgid "See the"
msgstr ""

#: src/preferences/aboutPage.js:174
msgid "GNU General Public License, version 2 or later"
msgstr ""

#: src/preferences/aboutPage.js:174
msgid "for details."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:54
msgid "Weather Provider"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:62
#, fuzzy
msgid "Temperature Unit"
msgstr "Temperatur"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:66
#, fuzzy
msgid "Pressure Unit"
msgstr "Tryk:"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:70
msgid "Wind Speed Units"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:75
msgid "Wind Direction by Arrows"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Choose whether to display wind direction through arrows or letters."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:80
msgid "City to be displayed"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:84
msgid "Actual City"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:92
msgid "OpenWeatherMap Multilingual Support (weather descriptions only)"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:100
#, fuzzy
msgid "Temperature in Panel"
msgstr "Temperatur"

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:104
msgid "Conditions in Panel"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:112
msgid "Conditions in Forecast"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:116
#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:120
msgid "Position in Panel"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:124
msgid "Horizontal position of menu-box."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:128
msgid "Refresh interval (actual weather)"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:132
msgid "Maximal length of the location text"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:136
msgid "Refresh interval (forecast)"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:140
msgid "Center forecastbox."
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:144
msgid "Always keep forecast expanded"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:148
msgid "Number of days in forecast"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:152
msgid "Maximal number of digits after the decimal point"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:156
msgid "Your personal API key from openweathermap.org"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:160
msgid "Use the extensions default API key from openweathermap.org"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:164
msgid "Your personal AppKey from developer.mapquest.com"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:168
msgid "Seconds to delay popup initialization and data fetch on the first load"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:172
msgid "Default width for the preferences window"
msgstr ""

#: schemas/org.gnome.shell.extensions.openweather.gschema.xml:176
msgid "Default height for the preferences window"
msgstr ""
